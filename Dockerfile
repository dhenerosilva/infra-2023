FROM golang:latest

COPY ./infra-2023 infra-2023

WORKDIR 'infra-2023'

RUN go mod download

RUN go get github.com/gofiber/fiber/v2

RUN go get github.com/google/uuid

RUN go get github.com/redis/go-redis/v9

RUN go build -o main .

CMD ["./main"]
