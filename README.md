# Desafio de Infra Equipe SIGA

## Execução parte 1 - obrigatória

Primeira coisa é buildar a image appsiga no diretório que se encontra o Dockerfile executando: docker build -t appsiga .

Após isso, devemos entrar na pasta /compose e executar o comando: docker-compose up
